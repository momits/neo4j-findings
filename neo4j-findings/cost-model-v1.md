Cost model of Neo4j
===================

Indexes
-------

Only single property indexes are supported at the moment
(see <https://github.com/neo4j/neo4j/blob/3.1/community/kernel/src/main/java/org/neo4j/kernel/impl/coreapi/schema/IndexCreatorImpl.java>).

See <http://nigelsmall.com/neo4j/index-confusion> for the differences between
legacy and schema indexes. Schema indexes are *only* available for nodes.

At the moment the schema indexes are Lucene indexes. Lucene is a library
built for full-text processing, which means that Lucene indexes are optimized
for full-text / fuzzy search techniques.
It is interesting that these fuzzy search features are neither available from
the Neo4j Cypher query frontend, nor the Java API, because this raises the
question whether Lucene indexes are the best choice for Neo4j.

Catalog information
-------------------

Neo4j stores the following catalog informations:

  - N(*)

    Total number of nodes.

  - N(l)

    Number of nodes of label l.

  - R(*)

    Total number of relationships.

  - R(t)

    Number of relationships of type t.

  - R(l_1, t, l_2)

    Number of relationships of type t from nodes with label l_1 to nodes with
    label l_2.

  - uniq(I)

    Fraction of unique keys of index I (called "index selectivity" in the docs).

  - size(I)

    Number of entries in the index I.

See <http://neo4j.com/docs/stable/query-schema-statistics.html> and especially
<https://github.com/neo4j/neo4j/blob/3.1/community/kernel/src/main/java/org/neo4j/kernel/impl/util/dbstructure/DbStructureCollector.java>.

Cost constants
--------------

The actual values for these constants should be retrieved from a set of test
runs.

  - Hit

    Cost of accessing a store.

  - Scan

    Cost of reading one block of data in a sequence from a store after it has
    been accessed.

  - Index

    Cost of accessing an index.

Cost approximations
-------------------

  - **AllNodesScan**

    Retrieves all nodes one by one from the node store.

    Hit + N(*) * Scan

  - **NodeByLabelScan**

    Retrieves all nodes with a specific label, using the label index.

    Hit + N(l) * Scan

  - **NodeIndexSeek**

    Retrieves all nodes which meet a certain property predicate, using an index.
    *Assumption*: The number of values is uniform for all keys.

    Index + 1 / uniq(I) * Hit

  - **NodeHashJoin**
    
    
    

