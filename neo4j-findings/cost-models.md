Cost models of minibase and Neo4j
=================================

Minibase...

  - does differentiate CPU and IO costs.
  - splits up cost calculation in several modules named after the
      corresponding operators.

Neo4j...

  - does not differentiate CPU and IO costs
  - has a test called "ActualCostCalculationTest.scala" which is used to
    estimate the constants in the cost model
  - assigns cost for the Hash join without checking cardinalities (potentially
    using the much larger relation for building the hash table instead of
    probing...)
  - assigns costs in one single module, very cluttered and disorganized.

How to proceed?
===============

 1. Understand the traversals made by Neo4j.
 2. Build a new cost model and document it properly.
