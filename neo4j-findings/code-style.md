Observations regarding the code style of Neo4j
==============================================

Many, many Interfaces
---------------------

I suspect the following practice:

  1. No matter what it is, specify an interface for it.
  2. Then add one or more implementations (often suffixed with *Impl*).

In SE this would be called *Interface-first*-methodology...


