What is an optimizer?
---------------------

Something that transforms logical query expressions in a physical query
execution plan.

Which approaches exist?
-----------------------

  1. **SystemR**
     Start with the relations, glue them together to locally optimal blocks,
     iterate.
     → Dynamic programming.

  2. **Starburst**
     Start with the whole query as QGM, then transform it heuristically.
     Then stick physical plans on the parts of the QGM.

  3. **Cascades**
     Start with the whole query. Perform optimization tasks according to promise
     and calculated cost, *on demand*.

Cascades
--------

## Groups

A set of logically equivalent multi-expressions.
Store a lower bound for the cost of the execution of the group.

## Multi-Expressions

An expression consisting of an operator that is applied on specific groups.

## Search space

An array of groups, that are in the search space ("a list of subproblems").
Duplicate detection is applied to avoid duplicate group exploration.

## Search context

A set of required properties and an upper bound of the cost.

## Tasks

Tasks have a context and a pointer to the parent task.
The following tasks exist:

  1. **Group optimization**
     Returns the best plan for executing this group given a search context.
     If the lower bound of the group exceeds the upper bound of the context,
     returns no result.
     If $<group, context>$ has already been optimized, returns the old result.
     Otherwise, new tasks are pushed: 
     One optimization task for every logical expression that is in the group.
     One optimization task for every physical expression that is in the group
     (input optimization).

  2. **Group exploration**
     Called on demand, when a rule requires to expand the set of equivalent
     expressions. Pushes an expression exploration task for every logical
     expression that is already in the group.

  3. **Expression exploration**
     Fires all transformation rules in the rule set that have not yet been
     fired for the expression.

  4. **Expression optimization**
     Fires all transformation and implementation rules in the rule set that have
     not yet been fired for the expression.

  5. **Input optimization**
     

  6. **Rule application**
     Applies a rule to a logical expression, generating new logical or physical
     expressions.
     Works by finding all pattern bindings for the rule on the expression, then
     checking whether the conditions for rule application are met.
     If so, all possible substitutes are generated and added into the search
     space.
     For every new logical substitute, an expression optimization task is
     pushed.
     For every new physical substitute, an input optimization task is pushed.
