---
title: Logical parts of a Cascades query optimizer for graph databases
author: Moritz Renftle
date: July 2016

classoption: table
---

Motivation: graph query languages
=================================

What is a graph database?
-------------------------

\colA{0.4\textwidth}

\includegraphics[width=\textwidth]{friend_hobby_graph.pdf}

\colB{0.5\textwidth}

A database using graphs for storing and querying information.

\begin{alertblock}{Goal:}
  Finding interesting subgraphs in a huge graph.
\end{alertblock}

>\includegraphics[width=0.7\textwidth]{friend_hobby_results.pdf}

\colEnd

Using SQL for querying graphs
-----------------------------

Every graph problem can be expressed in SQL.

```sql
SELECT c.CompanyName
FROM customers AS c
JOIN orders AS o ON (c.CustomerID = o.CustomerID)
JOIN order_details AS od ON (o.OrderID = od.OrderID)
JOIN products AS p ON (od.ProductID = p.ProductID)
WHERE p.ProductName = 'Chocolade'
```

Using a dedicated graph query language
--------------------------------------

However, specialized graph query languages can greatly improve
user productivity, being closer to the mental model of graphs.

\texttt{
\begin{tabularx}{\textwidth}{l@{ }l}
    \color{code-grey}
    {\color{code-green} MATCH} & (p:Product \{productName:{\color{code-blue}"Chocolade"}\}) \\
                               & ~~<-[:PRODUCT]-(:Order) \\
                               & ~~<-[:PURCHASED]-(c:Customer) \\
    \multicolumn{2}{l}{{\color{code-green} RETURN} c.companyName}
\end{tabularx}}

Designing a graph pipeline
==========================

neo4j and its current optimization pipeline
-------------------------------------------

The inventors of the mentioned Cypher query language have also implemented a
graph database called **neo4j**:

\begin{center}
  \input{neo4j_pipeline.tex}
\end{center}

neo4j...

  - accepts Cypher as query language
  - uses a non-relational storage engine, allowing graph traversals using
    physical node addresses
  - uses special physical operators to make graph operations faster

Replacing neo4j's optimizer
---------------------------

**Unfortunately, the neo4j optimizer does not always find a good plan.**

We want to build an optimizer based on the
\textcolor{mLightGreen}{Cascades optimizer framework} which represents the state
of the art in the relational world.

\begin{center}
  \input{neo4j_cascades_pipeline.tex}
\end{center}

Implementing the Cascades optimizer
===================================

Requirements for the Cascades framework
---------------------------------------

We need...

  1. a **data model** representing the graph database
  2. an **algebra** representing the result space of queries on the model
  3. a definition of the **logical properties** of operator results
  4. good **estimations** of how these logical properties change when
     operators are applied
  5. **transformation and implementation rules** that allow us to explore the
     search space of execution plans

Step 1: Data model
------------------

\input{step_1_data_model.tex}

Step 1: Data model
------------------

The database $\db$ is represented as a **property graph**.

\begin{center}
  \includegraphics[height=6.5cm]{friend_hobby_graph_detailed.pdf}
\end{center}

Step 2: Graph algebra
---------------------

\input{step_2_graph_algebra.tex}

Step 2: Graph algebra
---------------------

Our graph algebra $\galg$ operates on **sets of matched subgraphs** of the
database $\db$.

\colA{0.4\textwidth}

Database:

\vspace{0.2cm}
\includegraphics[width=\textwidth]{likes_graph.pdf}

\colB{0.4\textwidth}

Algebra expression:
\begin{overlayarea}{0.4\textwidth}{1cm}
    \only<1>{$\bigcirc_u$}
    \only<2>{$\phi_{u \leftarrow v}(\bigcirc_u)$}
    \only<3>{$\phi_{v \rightarrow w}(\phi_{u \leftarrow v}(\bigcirc_u))$}
\end{overlayarea}
\vspace{-0.4cm}
Result:
\begin{overlayarea}{0.4\textwidth}{4.4cm}
    \only<1>{\includegraphics[height=3.6cm]{likes_graph_nodes.pdf}}
    \only<2>{\includegraphics[height=4.32cm]{likes_graph_one_edge_matching.pdf}}
    \only<3>{\includegraphics[height=2.16cm]{likes_graph_two_edge_matching.pdf}}
\end{overlayarea}

\colEnd

Step 2: Graph algebra (operators)
---------------------------------

We can use the basic set operations $\Union{}{},\Intersection{}{}$.

Additionally, we want to have the following operators:

\vspace{1em}
\begin{overlayarea}{\textwidth}{4.5cm}
  \only<1>{
  \begin{tabularx}{\textwidth}{lX}
    $\GetNodes{v}{\db}$    & produces all single node subgraphs of $\db$ \\[0.5cm]
    $\Selection{x:L}{\inp},~ \Selection{e:T}{\inp}$ & keeps all subgraphs from $\inp$ where $x$ has all the labels in $L$ /
                                                      $e$ has one of the types in $T$\\[0.5cm]
    $\Selection{x.p = v}{\inp}$ & keeps all subgraphs of $\inp$ where $x$ has the property $p$ with
                                  value $v$
  \end{tabularx}}
  \only<2>{
  \begin{tabularx}{\textwidth}{lX}
    $\Expand{x}{X}{e}{E}{y}{Y}{\db}{\inp}$ & expands each subgraph in $\inp$ by
                                             adding new nodes using a relationship in $G$\\[0.5cm]
    $\NodeJoin{\inp_1}{\inp_2}$ & combines its two inputs by merging overlapping
                                  pairs of subgraphs
  \end{tabularx}
  }
\end{overlayarea}

Step 3: Logical properties
--------------------------

\input{step_3_logical_properties.tex}

Step 3: Logical properties
--------------------------

\begin{alertblock}{Recall:}
  Logical properties are {\bf metadata}. They are used to {\bf estimate} the
  costs of real data in the database.
\end{alertblock}

Step 3: Logical properties
--------------------------

The neo4j database has the following global logical properties in its
**system catalog**:

\begin{tabularx}{\textwidth}{ll}
  $\nodeLabels$    & set of optional node labels \\
  $\relTypes$      & set of relationship types \\
  $\properties$    & set of properties \\
  $N(l)$           & number of nodes of label $l$ \\
  $R(e)$           & number of relationships of type $e$ \\
  $R(l_1, e, l_2)$ & number of relationships of type $e$ from nodes \\
                   & with label $l_1$ to nodes with label $l_2$ \\
  $\has(l, p)$     & fraction of nodes with label $l$ that have the \\
                   & property $p$ \\
  $\sel(l, p)$     & probability that nodes with label $l$ have the \\
                   & property $p$ with a particular value
\end{tabularx}

Step 3: Logical properties
--------------------------

For a set of matched subgraphs $\inp$ we define the following **logical 
result properties**:

\vspace{5pt}
\begin{tabularx}{\textwidth}{ll}
  $\inp.\size$ & number of matched subgraphs \\[2pt]
  $\inp.\nodeVars,~ \inp.\relVars$ & two disjunct sets of variables used for \\
                             & matching nodes/rel.ships in the subgraphs \\[2pt]
  $\inp.l : \inp.\nodeVars \rightarrow \powSet{\nodeLabels}$ & a function assigning
                                                   node labels \\
                                                 & to node variables \\[2pt]
  $\inp.t : \inp.\relVars \rightarrow \powSet{\relTypes}$ & a function assigning
                                                      allowed edge types \\
                                                    & to relationship variables
\end{tabularx}

Step 4: Transformations of logical properties
---------------------------------------------

\input{step_4_logical_transformations.tex}

Step 4: Transformations of logical properties
---------------------------------------------

\begin{exampleblock}{Example: The transformation for the join operator}

\vspace{2em}
\centering
\input{logical_transformation.tex}

\end{exampleblock}

Step 4: Transformations of logical properties (example)
-------------------------------------------------------

\begin{exampleblock}{The label selection $\Selection{x:L}{\inp}$}

\vspace{1em}
\begin{overlayarea}{\textwidth}{5.5cm}
\only<1>{
  Keeps all subgraphs from $\inp$ where $x$ has all the labels in $L$.
}
\only<2>{
  % To which degree does adding node labels to x reduce the result size?

  % Same fraction as for all nodes 
  Idea: Estimate the reduction factor $f$ using the system catalog.

  \begin{align*}
    f ~:=~~ &\frac{
              \text{nodes in $\inp$ with labels } \inp.l(x) \cup L
            }{
              \text{nodes in $\inp$}
            } \\[8pt]
        \overset{\textbf{!}}{=}~~ &\frac{
              \text{nodes in $\db$ with labels } \inp.l(x) \cup L
            }{
              \text{nodes in $\db$ with labels } \inp.l(x)
            } \\[8pt]
        =~~ &\frac{N(\inp.l(x) \cup L)}{N(\inp.l(x))}
  \end{align*}
}

\only<3>{
  {\bf Available:} $N(l)$ for single labels $l$

  \vspace{1em}
  {\bf Needed:} $N(L)$ for a set of labels $L$
}
\only<4>{
  Observe that
  \begin{align*}
    \Selection{x:L}{\bigcirc_x} = \underset{l \in L}{\bigcap} \Selection{x:\{l\}}{\bigcirc_x}
  \end{align*}

  where $~|\Selection{x:\{l\}}{\bigcirc_x}| = N(l)~$ is available from
  the system catalog.

  \vspace{8pt}
  We get an upper bound:
  \begin{align*}
    N(L) \leq \min \lb\{ N(l) ~:~ l \in L \}\rb
  \end{align*}
}
\only<5>{
  {\bf Assumption: Node labels are often in semantical subset relations
   (e.g. $\texttt{Student} \subseteq \texttt{Person}$).}

  \vspace{1em}
  Avoid underestimation, use the upper bound.
  \begin{align*}
    N(L) := \min \lb\{ N(l) ~:~ l \in L \}\rb
  \end{align*}

  Now we can calculate
  \begin{align*}
    \outp.\size = \inp.\size \cdot \frac{N(\inp.l(x) \cup L)}{N(\inp.l(x))}
  \end{align*}
}
\only<6>{
  Update the node label function.

  \vspace{-8pt}
  \begin{align*}
    \outp.l(x) = \inp.l(x) \cup L
  \end{align*}

  The other logical properties remain unchanged.
}

\end{overlayarea}
\end{exampleblock}

Step 4: Transformations of logical properties - Review
------------------------------------------------------

We have definitions of the logical properties transformations for each
operator in

\vspace{-1em}
\begin{align*}
  \left \{
    \GetNodes{v}{\db}, ~~\Selection{x:L}{\inp}, ~~\Selection{x.p = v}{\inp},
    ~~\Expand{x}{X}{e}{E}{y}{Y}{\db}{\inp}, ~~\NodeJoin{\inp_1}{\inp_2}
  \right \}
\end{align*}

\vspace{1em}
\begin{overlayarea}{\textwidth}{1cm}
  \only<2>{
    {\bf Are the definitions sensible?}
  }
\end{overlayarea}

Step 4: Transformations of logical properties - Review
------------------------------------------------------

There are some convenient equivalences maintained by the transformations.

\vspace{1em}
\begin{overlayarea}{\textwidth}{4.5cm}
  \only<2>{
  \begin{tabularx}{\textwidth}{ll}
  (i) & Selections based on labels and types are \\
      & {\bf idempotent} and {\bf commutative}:
  \end{tabularx}
  % Nice thing: We don't need to introduce rules to get these equivalences -
  % they are preserved by our logical properties definitions.

  \begin{align*}
    \Selection{x:X_2}{\Selection{x:X_1}{\inp}} &\logeq \Selection{x:X_1 \cup X_2}{\inp} \\
    \Selection{e:T_2}{\Selection{e:T_1}{\inp}} &\logeq \Selection{e:T_1 \cap T_2}{\inp}
  \end{align*}
  }

  \only<3>{
  \begin{tabularx}{\textwidth}{ll}
  (ii) & The expand operator is {\bf compatible} \\
       & with an inner label selection:
  \end{tabularx}

  \begin{align*}
    \Expand{x}{X_2}{e}{\{ t \}}{y}{Y}{\db}{\Selection{x:X_1}{\inp}} \logeq
    \Expand{x}{X_1 \cup X_2}{e}{\{ t \}}{y}{Y}{\db}{\inp}
  \end{align*}
  }
\end{overlayarea}



Step 5: Defining rules for exploring the search space
-----------------------------------------------------

\input{step_5_rules.tex}

Step 5: Defining rules for exploring the search space
-----------------------------------------------------




