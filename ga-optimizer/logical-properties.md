\newcommand{\lb}{\left(}
\newcommand{\rb}{\right)}

Fundamentals of the graph algebra
=================================

\newcommand{\nodeVars}{V_\texttt{nodes}}
\newcommand{\edgeVars}{V_\texttt{edges}}
\newcommand{\galg}{\mathcal{A}}
\renewcommand{\G}{G}
\newcommand{\operators}{\texttt{ops}}

The database / knowledge base $G$ is represented as a directed graph with
optional node labels and edge types.

The graph algebra $\galg$ operates on *sets of matched subgraphs* of $\G$ which
have a common structure.
This structure can be specified using a *pattern graph*, where nodes and
edges are from two distinct sets of variables.
A subgraph $\mu$ of $\G$ matches this pattern graph, iff

  - there exists a surjective function from the node variables in the pattern
    graph to the nodes of $\mu$
  - there exists a bijective function from the edge variables in the pattern
    graph to the edges of $\mu$

The graph algebra can thus be defined as

\begin{align*}
    \galg(\G) := \{
                    \{ \mu ~:~ \mu \text{ is subgraph of } \G \text{ and matched by } p \}
                    ~:~
                    p \text{ is pattern graph }
                 \}
\end{align*}

where $\nodeVars$ and $\edgeVars$ are the sets of node and edge variables for
the pattern graphs.

Operators on the algebra are functions which consume an arbitrary number of sets
of matched subgraphs and produce a new set of matched subgraphs:

\begin{align*}
    \operators(G) := \{ f ~:~ \galg(\G)^k \rightarrow \galg(G), ~k \in \mathbb{N}_0\}
\end{align*}

Logical properties
==================

\newcommand{\nodeLabels}{\mathbb{L}}
\newcommand{\edgeTypes}{\mathbb{T}}
\newcommand{\properties}{\mathbb{P}}
\newcommand{\powSet}[1]{\mathcal{P}\left(#1\right)}
\newcommand{\size}{\texttt{size}}
\newcommand{\has}{\texttt{has}}
\newcommand{\uniq}{\texttt{uniq}}
\newcommand{\logeq}{~\underset{\texttt{log}}{\equiv}~}

The database has some *global logical properties* (often referred to as
*catalog information*):

  - a set of optional node labels $\nodeLabels$ and the universal node label $*$
    that every node has.

    We call
    $S(\nodeLabels) := \{ L \cup \{ * \} ~:~ L \in \powSet{\nodeLabels} \}$
    the set of *valid sets of node labels*.

  - a set of edge types $\edgeTypes$ and the universal relationship type $*$
    that every relationship has

    We call $S(\edgeTypes) := \powSet{\edgeTypes} \cup \{ \{ * \} \}$
    the set of *valid sets of edge types*.

  - a set of properties $\properties$
    
    where $range(p)$ is the value range of any property $p \in \properties$

  - $N(l)$

    number of nodes of label $l$

  - $R(e)$

    number of relationships of type $e$

  - $R(l_1, e, l_2)$

    number of relationships of type $e$ from nodes with label $l_1$ to nodes
    with label $l_2$

  - $\has(l, p)$

    fraction of nodes with label $l$ that have the property $p$

  - $\uniq(l, p)$

    fraction of unique values of property $p$ at nodes with label $l$


A set of matched subgraphs $M$ has the following *local logical properties*:

  - $M.\size$, the number of matched subgraphs

  - two disjunct sets of variables $M.\nodeVars$ and $M.\edgeVars$
    used for matching nodes/edges in the subgraphs

  - a function
   
    $M.l : M.\nodeVars \rightarrow S(\nodeLabels)$

    assigning node labels to node variables

  - a function

    $M.t : M.\edgeVars \rightarrow S(\edgeTypes)$

    assigning allowed edge types to relationship variables

We write $M_1 \logeq M_2$ iff the local logical properties of the sets of
subgraphs $M_1$ and $M_2$ are identical.

Operators
=========

\newcommand{\inp}{M}
\newcommand{\outp}{M_{out}}

In the following considerations, let $\inp$ denote an input set of
subgraphs and $\outp$ the output set of subgraphs for the respective operator.



\newcommand{\GetNodes}[2]{\bigcirc_{#1}(#2)}

The GetNodes-operator $\GetNodes{v}{\G}$
----------------------------------------

The get nodes operator produces the set of all single node subgraphs.

$\outp.\size = N(*)$

$\outp.\nodeVars = \{ v \}$

$\outp.\edgeVars = \emptyset$

$\outp.l = \emptyset$

$\outp.t = \emptyset$



\newcommand{\Selection}[2]{\underset{#1}{\sigma}(#2)}

The NodeLabelSelection-operator $\Selection{x:L}{\inp}$
-------------------------------------------------------

The label selection keeps all subgraphs from $\inp$ where the node variable $x$
has all the node labels in $L \in S(\nodeLabels)$.

It holds that

$\Selection{x:L}{\inp} = \underset{l \in \inp.l(x) \cup L}{\bigcap} \Selection{x:\{l\}}{\inp}$

and $~\min \lb\{ N(l) ~:~ l \in \inp.l(x) \cup L \}\rb~$ is an upper bound for
the result size. Assuming that node labels do often overlap, it may be a good
idea to simply use this upper bound for the result size estimation [^1].

From now on we set $N(X) := \min \lb\{ N(l) ~:~ l \in X \}\rb$.

We calculate the result size as
\begin{align*}
    \outp.\size = \inp.\size \cdot \frac{N(\inp.l(x) \cup L)}{N(\inp.l(x))}
\end{align*}

and set

$\outp.l(x) = \inp.l(x) \cup L$

The other logical properties remain unchanged.

This definition of the label selection has the property
\begin{align*}
    \Selection{x:X_2}{\Selection{x:X_1}{\inp}} \logeq \Selection{x:X_1 \cup X_2}{\inp}
\end{align*}

[^1]: Query Processing and Optimization in Graph Databases, Dr. Andrey Gubichev,
p. 128



The RelationshipTypeSelection-operator $\Selection{e:T}{\inp}$
--------------------------------------------------------------

The type selection keeps all subgraphs from $\inp$ where the relationship
variable $e$ has one of the relationship types in $T \in S(\edgeTypes)$.

For simplicity, we update the relationship type map first:

$\outp.t(e) = \begin{cases}
                T \text {, if } \inp.t(e) = \{ * \} \\
                \inp.t(e) \cap T, \text{ otherwise } 
              \end{cases}$.

Because every edge has only one type, it holds that

$\Selection{e:T}{\inp} = \underset{t \in \outp.t(e)}{\bigcup} \Selection{e:\{t\}}{\inp}$

and $~\sum_{t \in \outp.t(e)} R(t)~$ is an upper bound for the result
size. We want to use this upper bound to calculate the estimated result size.

From now on we set $R(E) := \sum_{t \in E} R(t)$.

We get
\begin{align*}
    \outp.\size = \inp.\size \cdot
                      \frac{
                          R(\outp.t(e))
                      }{
                          R(\inp.t(e))
                      }
\end{align*}

The other logical properties remain unchanged.

From this definition follows the property
\begin{align*}
    \Selection{e:T_2}{\Selection{e:T_1}{\inp}} \logeq \Selection{e:T_1 \cap T_2}{\inp}
\end{align*}



\newcommand{\Expand}[8]{\phi_{#1:#2 \overset{#3:#4}{\longrightarrow} #5:#6}(#7, #8)}

The Expand-operator $\Expand{x}{X}{e}{E}{y}{Y}{\G}{\inp}$
---------------------------------------------------------

The expand operator expands each matched subgraph in $\inp$ by adding new nodes
using an outgoing (or incoming) relationship in $G$.
We require that

  1. $X,Y \in S(\nodeLabels)$
  2. $E \in S(\edgeTypes)$
  3. $x \in \inp.\nodeVars$ and $e \not \in \inp.\edgeVars$

For a subgraph to be expanded, the start nodes of the expansion must have all
the labels in $X$, the relationship must be of a type in $E$ and the end nodes
must have all the labels in $Y$.

To begin with, we unite already known node labels with the new ones set by the
expansion, as we want to include all availabe information in our calculation.
If $x = y$ the expand finds loops on $x$ and we set

$X' := Y' := \inp.l(x) \cup X \cup Y$

Otherwise

$X' := \inp.l(x) \cup X ~~~~ Y' := \inp.l(y) \cup Y$

We can now make use of our existing estimations for the node label and
relationship type selections to estimate the result size of the expansion.
Firstly, we may split the cardinality estimation
for every single edge type $t \in E$ and sum up the result sizes afterwards.

Secondly, it holds that

\begin{align*}
    \Expand{x}{X}{e}{\{ t \}}{y}{Y}{\G}{\inp} &= \Expand{x}{X'}{e}{\{ t \}}{y}{Y'}{\G}{\inp} \\
                                              &= \underset{l_1 \in X', l_2 \in Y'}{\bigcap}\Expand{x}{\{ l_1 \}}{e}{\{ t \}}{y}{\{ l_2 \}}{\G}{\inp}
\end{align*}

Consequently, $~\min \lb \{ R(l_1, t, l_2) ~:~ l_1 \in X', l_2 \in Y' \} \rb~$
is an upper bound for the size of $~\Expand{x}{X}{e}{\{ t \}}{y}{Y}{\G}{\inp}~$.
Once more, we want to use this upper bound to calculate the estimated result
size.

We set
\begin{align*}
    R(X', E, Y') := \sum_{t \in E} \min \lb \{ R(l_1, t, l_2) ~:~ l_1 \in X', l_2 \in Y' \} \rb
\end{align*}

and end up with

\begin{align*}
    \outp.\size = \inp.\size \cdot
    \frac{
      R(X', E, Y')
    }{
      N(\inp.l(x))
    }
\end{align*}

For the expansion using incoming edges we can simply use $R(Y', E, X')$.

Finally, it holds that

$\outp.\nodeVars = \inp.\nodeVars \cup \{ y \}$

$\outp.\edgeVars = \inp.\edgeVars \cup \{ e \}$

$\outp.l(x) = X'$

$\outp.t(e) = E$

$\outp.l(y) = Y'$


As a result of this definition, we get the property
\begin{align*}
    \Expand{x}{X_2}{e}{\{ t \}}{y}{Y}{\G}{\Selection{x:X_1}{\inp}} \logeq
    \Expand{x}{X_1 \cup X_2}{e}{\{ t \}}{y}{Y}{\G}{\inp}
\end{align*}



\newcommand{\NodeJoin}[2]{#1 \bowtie #2}

The Join-operator $\NodeJoin{\inp_1}{\inp_2}$
---------------------------------------------

The join operator combines its two inputs by merging overlapping pairs of
subgraphs. A pair of subgraphs does overlap, if both subgraphs share a node.

If $\inp_1.\nodeVars \cap \inp_2.\nodeVars = \emptyset$, the result will be
empty.

Otherwise, we estimate the result size by calculating the probability, that a
pair of subgraphs from the inputs does overlap.
Because the Neo4j catalog does not provide more information, we have to use the
assumption, that node labels are independent from each other and thus irrelevant
for the join cardinality estimation.

As a result, we get

\begin{align*}
  p := |\inp_1.\nodeVars \cap \inp_2.\nodeVars)| \cdot \frac{1}{N(*)}
\end{align*}

and

$\outp.\size = \inp_1.\size \cdot \inp_2.\size \cdot p$

The other logical output properties are as follows:

$\outp.\nodeVars = \inp_1.\nodeVars \cup \inp_2.\nodeVars$

$\outp.\edgeVars = \inp_1.\edgeVars \cup \inp_2.\edgeVars$

$\forall x \in \outp.\nodeVars ~:~ \outp.l(x) = \inp_1.l(x) \cup \inp_2.l(x)$

$\outp.t = \inp_1.t \cup \inp_2.t$



The NodePropertySelection-operator $\Selection{x.p = v}{\inp}$
--------------------------------------------------------------

The node property selection keeps all subgraphs of $\inp$ where a node is
matched by $x$ and this node has the property $p$ with value $v$.

The fraction of nodes in $M$ which have the property $p$ can be estimated as
\begin{align*}
    f := \frac{\sum_{l \in M.l(x)}\has(l,p)}{|M.l(x)|}
\end{align*}

The expected number of selected subgraphs is

\begin{align*}
    \outp.\size = \inp.\size \cdot f \cdot \frac{1}{|range(p)|}
\end{align*}

The other logical properties remain unchanged.



The RelationshipPropertySelection-operator $\Selection{e.p = v}{\inp}$
----------------------------------------------------------------------

The relationship property selection keeps all subgraphs of $\inp$ where a
relationship is matched by $e$ and this relationship has the property $p$ with
value $v$.

Because we do not know from the global logical properties how many relationships
have the property $p$, we simply assume that $p$ is present for all matched
relationships $e$. Thus, the expected number of selected subgraphs is

\begin{align*}
    \outp.\size = \inp.\size \cdot \frac{1}{|range(p)|}
\end{align*}

The other logical properties remain unchanged.



Logical properties API
======================

Logical properties should be immutable snapshots of the logical state of a set
of subgraphs.

The Java class signature could look like this:

```java
public class GALogicalProperties implements LogicalProperties {
    public GALogicalProperties(Map<String, Set<Integer>> nodeLabelMap,
                               Map<String, Set<Integer>> relationshipTypeMap,
                               double matchedSubgraphs) {}

    public Map<String, Set<Integer>> getNodeLabelMap() {}

    public Map<String, Set<Integer>> getRelationshipTypeMap() {}

    public double getSize() {}

    public Set<String> getNodeVariables() {}

    public Set<String> getRelationshipVariables() {}

    public Set<Integer> getLabels(String variable) {}

    public Set<Integer> getTypes(String variable) {}

    public boolean anyNodeMatchedBy(String variable) {}

    public boolean anyRelationshipMatchedBy(String variable) {}
}
```

The methods having maps or sets as return type return only immutable views on
these mutable internal data structures.

If *P* are the Java logical properties of a set of subgraphs $M$, then

  - *P.getNodeLabelMap()* $= M.l$
  - *P.getRelationshipTypeMap()* $= M.t$
  - *P.getSize()* $= M.\size$

The rest of *P*'s signature is convenience methods.



