This repository has two purposes:

1. A collection of findings about the internal structure of Neo4j
-----------------------------------------------------------------


2. Development of a Cascades query optimizer using a new graph algebra
----------------------------------------------------------------------


As the optimizer is designed to be used in Neo4j, the second goal of this
repository depends on the first one.

All informations are provided as pandoc-compilable Markdown files.
